#!/usr/bin/env python3
import json
import logging
from argparse import ArgumentParser, FileType, Namespace
from collections import defaultdict
from sys import exit

import gitlab

LOGGER=logging.getLogger(__file__)
LOGGER.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
LOGGER.addHandler(ch)


def args() -> Namespace:
    parser = ArgumentParser(
        description="Dodaje wykryte defekty w formacie Code Climate do recenzji w GitLabie w formacie komentarzy"
    )
    parser.add_argument("--url", help="Adres API GitLaba", required=True, type=str)
    parser.add_argument(
        "--token", help="Żeton uwierzytelniający", required=True, type=str
    )
    parser.add_argument("--project-id", help="Unikalny ID projektu w GitLabie", required=True, type=int)
    parser.add_argument("--mergerequest-id", help="Unikalny w ramach projektu ID recenzji", required=True, type=int)
    parser.add_argument(
        "--input",
        help="Plik z raportem Code Climate",
        required=True,
        type=FileType("r"),
        default="-"
    )

    return parser.parse_args()


def main():
    cmd = args()
    data = json.load(cmd.input)
    publish_comments(data, cmd.url, cmd.token, cmd.project_id, cmd.mergerequest_id)


def get_diff_discussions(mr):
    diff_discussions = defaultdict(set)

    for discussion in mr.discussion.list(all=True):
        parent_note = discussion.attributes["notes"][0]
        if parent_note["type"] != "DiffNote":
            continue

        position = parent_note["position"]
        path = position.get("new_path") or position["old_path"]
        line = position.get("new_line") or position["old_line"]
        diff_discussions[f"{path}":{line}].add(parent_note["body"])
    return diff_discussions


def create_comment_payload(defect, last_diff):
    fingerprint = defect["fingerprint"]
    rule = defect["check_name"]
    desc = defect["description"]
    path = defect["location"]["path"]
    line = defect["location"]["positions"]["begin"]["line"]
    body = f"{fingerprint}: {rule}\n\n{desc}"
    return {
        "body": body,
        "position": {
            "position_type": "text",
            "base_sha": last_diff.base_commit_sha,
            "head_sha": last_diff.head_commit_sha,
            "start_sha": last_diff.start_commit_sha,
            "new_path": path,
            "new_line": line,
        },
    }


def publish_comments(
        defects: list[dict], gitlab_url: str, token: str, proj_id: int, mr_id: int
):
    gl = gitlab.Gitlab(gitlab_url, private_token=token)
    gl.auth()

    mr = gl.projects.get(proj_id).mergerequests.get(mr_id)
    diff_discussions = get_diff_discussions(mr)

    last_diff = mr.diffs.list(per_page=1)[0]
    for defect in defects:
        payload = create_comment_payload(defect, last_diff)
        path = payload["position"]["new_path"]
        line = payload["position"]["new_line"]

        if payload["body"] in diff_discussions[f"{path}:{line}"]:
            LOGGER.debug(
                "Skipping already reported defects %s at file %s:%s",
                defect["check_name"],
                path,
                line
            )
            continue

        discussion = mr.discussions.create(payload)
        LOGGER.debug("Created discussion %s", discussion)
    exit(int(len(defects) != 0))


if __name__ == '__main__':
    main()
