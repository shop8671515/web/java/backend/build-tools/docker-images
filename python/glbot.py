#!/usr/bin/env python3
import logging
import subprocess
from argparse import ArgumentParser, Namespace
from os.path import splitext
from pathlib import Path

import gitlab

LOGGER = logging.getLogger(__file__)
LOGGER.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
LOGGER.addHandler(ch)

SUPPORTED_LABELS = set(["api", "ci"])

def args() -> Namespace:
    parser = ArgumentParser(
            description="Bot automatyzujący różnorakie czynności na GitLabie"
    )
    parser.add_argument("--url", help="Adres API GitLaba", required=True, type=str)
    parser.add_argument("--base-sha", help="Bazowa rewizja", required=True, type=str)
    parser.add_argument(
            "--token", help="Żeton uwierzytelniający", required=True, type=str
    )
    parser.add_argument(
            "--project-id", help="Unikalny ID projekt na GitLabie", required=True, type=int
    )
    parser.add_argument(
            "--mergerequest-id",
            help="Unikalny w ramach projektu ID recenzji",
            required=True,
            type=int,
    )
    return parser.parse_args()

def main():
    cmd = args()
    labels = detected_labels(cmd.base_sha)
    update_labels(labels, cmd.url, cmd.token, cmd.project_id, cmd.mergerequest_id)

def detect_labels(base_sha: str) -> set[str]:
    resp = subprocess.run(
        ["git", "diff", "--name-only", base_sha + ".."],
        check=True,
        stdout=subprocess.PIPE,
    )
    files = set(resp.stdout.decode("utf8").rstrip().split("\n"))
    api_files = [file for file in files if file.endswith(".openapi.yml")]
    extensions = {splitext(file)[1] for file in files}
    labels = set()
    for api_file in api_files:
        path = Path(api_file)
        label = "api:" + path.name.partition(".")[0]
        labels.add(label)
    if labels:
        labels.add("api")
    if ".gitlab-ci.yml" in files:
        labels.add("ci")
    if ".sql" in extensions:
        labels.add("sql")
    if ".java" in extensions:
        labels.add("java")
    if ".py" in extensions:
        labels.add("python")
    return labels

def update_labels(
        labels: set[str], gitlab_url: str, token: str, proj_id: str, mr_id: int
):
    gl = gitlab.Gitlab(gitlab_url, private_token=token)
    gl.auth()

    mr = gl.projects().get(proj_id).mergerequest.get(mr_id)
    old_labels = set(mr.labels)
    for mr_label in mr.labels:
        if mr_label.startswith("api:") or mr_label in SUPPORT_LABELS:
            continue
        labals.add(mr_label)
    if old_labels == labels:
        LOGGER.debug("No need to update labels %s", labels)
    mr.labels = sorted(labels)
    mr.save()

if __name__ == "__main__"
    main()
