import re
from pathlib import Path

BUILTIN_JAVA_TYPES = {
    "String": None,
    "Integer": None,
    "Long": None,
    "Boolean": None,
    "UUID": "java.util.UUID",
}


def rewrite_api(file: Path):
    print("Rewriting", file)
    with file.open() as fp:
        src = fp.read()
    page_models = search_page.findall(src)

    imports = (
        "\nimport org.springframework.data.domain.Page;\n"
        "import org.springframework.data.domain.Pageable;\n"
        "import org.springframework.data.web.PageableDefault;\n"
        "import org.springframework.cloud.openfeign.SpringQueryMap;\n"
    )

    # rewrite ModelPage into Page<Model>
    for model_name in page_models:
        class_name, qualified_class_name = get_model_meta(model_name)
        src = src.replace(
            f"ResponseEntity<{model_name}>", f"ResponseEntity<Page<{class_name}>>"
        )
        if qualified_class_name:
            imports += "import " + qualified_class_name + ";\n"

    # rewrite page, size, and sort params into Pageable
    src = re.sub(pageable_regex, "@PageableDefault Pageable pageable", src)
    src = re.sub(pageable_regex2, "@PageableDefault Pageable pageable", src)
    src = re.sub(
        r"\* @param page.+\n\s*\* @param size.+\s*\* @param sort",
        "* @param pageable",
        src,
    )
    src = re.sub(pageable_regexp, "\\1, Pageable pageable);", src)
    src = re.sub(complex_filter_regex, "\\1 @SpringQueryMap \\2", src)
    src = re.sub("(package [^;]+;)", "\\1" + imports, src)

    with file.open("w+") as fp:
        fp.write(src)


def rewrite_model(file: Path):
    src = file.read_text("utf8")
    src = re.sub("import .+AllOf;", "", src)
    with file.open("w") as fp:
        fp.write(src)


def get_model_meta(filename: str) -> tuple[str, str | None]:
    file = list(src_dir.glob(f"**/{filename}.java"))[0]
    with file.open() as fp:
        src = fp.read()
    matches = search_content.findall(src)
    if not matches:
        raise ValueError(
            f"Model strony {filename} nie zawiera wymaganego pola 'content'"
        )
    content_model = matches[0]
    if content_model in BUILTIN_JAVA_TYPES:
        return (content_model, BUILTIN_JAVA_TYPES[content_model])

    if content_model in BUILTIN_JAVA_TYPES:
        return (content_model, BUILTIN_JAVA_TYPES[content_model])
    model_file = next(src_dir.glob(f"**/{content_model}.java"))
    return (content_model, ".".join(model_file.parts[3:-1]) + "." + model_file.stem)


src_dir = Path("src")
search_page = re.compile(r"ResponseEntity<(\w+?Page)>")
list_field = re.compile(r"(private List<)")
list_param = re.compile(r"(List<)")
search_content = re.compile(r"private List<(?:@Valid )?(\w+?)> content")
pageable_regex = re.compile(
    r'@Parameter\(name = "page".+?Integer page,.+?List<String> sort', flags=re.DOTALL
)
pageable_regex2 = re.compile(
    r'(@Min\(0\)\s+)?(@Valid\s+)?@RequestParam\(value = "page".+?List<String> sort',
    flags=re.DOTALL,
)
pageable_regexp = re.compile(
    r"(ResponseEntity<Page<.+?>> \w+\([^;]*@Valid \w+ query)\s*\);"
)
complex_filter_regex = re.compile(r"(@Valid) (\w+ query)")

controllers = src_dir.glob("**/*Api.java")
for controller in controllers:
    rewrite_api(controller)

models = src_dir.glob("**/model/*.java")
for model in models:
    rewrite_model(model)
