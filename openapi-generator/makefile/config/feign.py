import re
from pathlib import Path
from tree_sitter import Language, Node, Parser

def main():
    java_so = Path(__file__).parent / "treesitter" / "java.so"
    if not java_so.exists():
        java_so = Path("/opt/linters/treesitter/java.so")

    java_language = Language(str(java_so), "java")

    java_parser = Parser()
    java_parser.set_language(java_language)
    src_dir = Path("src")
    controllers = src_dir.glob("**/*Api.java")
    for controller in controllers:
        rewrite_api(controller, java_parser, java_language)

def modify_method(method_node: Node, with_headers: bool, replace_file: bool):
    for child in method_node.children:
        if child.type == "block":
            return False, ""
    doc_node = method_node.prev_sibling
    doc = output_doc(doc_node, with_headers)
    method = [doc, "\n"]
    returns_file = False
    for child in method_node.children:
        parts: list[str] = []
        returns_file |= output_method(child, with_headers, replace_file, parts)
        method.extend(parts)

    method.append("\n")
    return returns_file, "".join(method)

def output_doc(node: Node, with_headers: bool):
    text = node.text.decode("utf8")
    if not with_headers:
        return text
    parts = text.split("\n")
    if "@return" in parts[-2]:
        parts.insert(-2, "  * @param headers Mapa z nagłówkami. (optional)")
    else:
        parts.insert(-1, "  * @param headers Mapa z nagłówkami. (optional)")
    return "\n".join(parts)

def output_method(
    node: Node, with_headers: bool, replace_file: bool, parts: list[str]
) -> bool:
    uses_file = False
    if node.type == "modifiers":
        parts.append(node.text.decode("utf8"))
        parts.append("\n")
    elif node.type == "type_identifier":
        text = node.text.decode("utf8")
        parts.append(text)
        parts.append(" ")
    elif node.type == "identifier":
        text = node.text.decode("utf8")
        if replace_file:
            if text.endswith("WithHttpInfo"):
                text = text.removeSuffix("WithHttpInfo") + "StreamWithHttpInfo"
            else:
                text += "Stream"
        if with_headers:
            if text.endswith("WithHttpInfo"):
                text += "AndHeaders"
            else:
                text += "WithHeaders"
        parts.append(text)
        parts.append(" ")
    elif node.type == "formal_parameters":
        has_params = len(node.children) != 2
        for param in node.children:
            if param.type == ")":
                if with_headers:
                    if has_params:
                        parts.append(", @HeaderMap Map<String, Object> headers)")
                    else:
                        parts.append("@HeaderMap Map<String, Object> headers)")
                else:
                    parts.append(")")
            elif param.type == "formal_parameter":
                for arg in param.children:
                    if arg.type == "type_identifier":
                        text = arg.text.decode("utf8")
                        if text == "File":
                            uses_file = True
                            if replace_file:
                                text = "MetadataSource"
                        parts.append(text)
                        parts.append(" ")
                    else:
                        parts.append(arg.text.decode("utf8"))
                        parts.append(" ")
            else:
                parts.append(param.text.decode("utf8"))
                parts.append(" ")
    else:
        parts.append(node.text.decode("utf8"))
        parts.append(" ")
    return uses_file

def rewrite_api(api: Path, java_parser: Parser, java_language: Language):
    src = api.read_text()
    tree = java_parser.parse(src.encode("utf8"))
    imports = (
        "\nimport feign.HeaderMap;\n"
        "import java.io.InputStream;\n"
        "import java.io.OutputStream;\n"
        "import pl.com.bit.common.feign.metadata;\n"
    )
    src = re.sub("(package [^;]+;)", "\\1" + imports, src)

    root = tree.root_node
    method_query = java_language.query("(method_declaration) @capture")
    nodes = method_query.captures(root)
    methods = []
    for node, _ in nodes:
        with_file, body = modify_method(node, with_headers=True, replace_file=False)
        methods.append(body)
        methods.append("\n")
        if with_file:
            _, body = modify_method(node, with_headers=True, replace_file=True)
            methods.append(body)
            methods.append("\n")
            _, body = modify_method(node, with_headers=False, replace_file=True)
            methods.append(body)
            methods.append("\n")

    generated_code = "".join(methods)
    index = src.rfind("}")
    src = src[: index - 1] + generated_code + src[index:]
    api.write_text(src)

if __name__ == "__main__":
    main()







































