[apiInner.mustache](./apiInner.mustache) :: [wersja 5.0.0](https://github.com/OpenAPITools/openapi-generator/blob/v5.0.0/modules/openapi-generator/src/main/resources/typescript-axios/apiInner.mustache) z poprawkami z wersji [6f5076e](https://github.com/OpenAPITools/openapi-generator/blob/d7d5e53f2bad69cdb3dffae35eef1f3f1090a22c/modules/openapi-generator/src/main/resources/typescript-axios/apiInner.mustache).

Dodano poprawkę dla niepoprawnej obsługi explode: true w parametrach GET.
https://github.com/OpenAPITools/openapi-generator/issues/10438#issuecomment-1134719275

[urls.mustache](./urls.mustache) :: generuje plik urls.js, który zawiera stałe
w formie `<nazwa operacji> = "<uri operacji>"`.
