## [pojo.mustache](pojo.mustache)

[Wersja 7417432a54](https://github.com/OpenAPITools/openapi-generator/blob/7417432a54124988efdffad3725a631694e34c85/modules/openapi-generator/src/main/resources/Java/pojo.mustache).

### Poprawki

- dodanie `@NotNull` na polach i setterach.
- ignorowanie w toString pól z `format: password`.
