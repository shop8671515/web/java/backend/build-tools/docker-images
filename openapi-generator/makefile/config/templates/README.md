# Poprawione szablony [openapi-generator](https://github.com/OpenAPITools/openapi-generator)

Dodany szablon powinien zawierać informację o rewizji na której bazuje i
informacje o zmianach, które zostały wprowadzone (jeżeli szablon pochodzi z
nowszej wersji generatora, który zawiera poprawkę, wystarczy stały odnośnik do
pliku i opis błędu, który jest poprawiony).