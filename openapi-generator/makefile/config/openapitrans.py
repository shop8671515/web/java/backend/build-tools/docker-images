from argparse import ArgumentParser, ArgumentTypeError, BooleanOptionalAction, Namespace
from pathlib import Path
from typing import Any

from ruamel.yaml import YAML

def readable_file_path(filename: str) -> Path:
    path = Path(filename)
    if not path.is_file():
        raise ArgumentTypeError(f"{filename} nie istnieje albo nie jest plikiem")
    return path

def cli() -> Namespace:
    parser = ArgumentParser(description="Transformuje specyfikację OpenAPI")
    parser.add_argument(
        "-i",
        "--input",
        required=True,
        type=readable_file_path,
        help="Wejściowa specyfikacja",
    )
    parser.add_argument(
        "-o", "--output", required=True, type=Path, help="Wynikowy plik"
    )
    parser.add_argument(
        "--explode-get-parameters",
        action=BooleanOptionalAction,
        help="Czy usunąć parametry stronicowania?",
    )
    parser.add_argument(
        "--remove-pageable-parameters",
        action=BooleanOptionalAction,
        help="Czy usunąć parametry stronicowania?",
    )
    return parser.parse_args()

def leafs(d: dict):
    for k, v in d.items():
        if isinstance(v, dict):
            yield from leafs(v)
        elif isinstance(v, list):
            for item in v:
                if isinstance(item, dict):
                    yield from leafs(item)
                else:
                    yield k, item
        else:
            yield k, v

def parse_openapi(specs: dict[Path, dict], filename: Path):
    spec = parse_yaml(filename)
    specs[filename.absolute()] = spec
    spec_dir = filename.parent
    for k, v in leafs(spec):
        if k == "$ref":
            filename = v.partition("#")[0]
            if filename == "":
                continue
            sub_spec = (spec_dir / filename).absolute()
            if sub_spec in specs:
                continue
            parse_openapi(specs, sub_spec)

def parse_yaml(filename: Path) -> dict[str, Any]:
    yaml = YAML()
    with filename.open("r", encoding="utf8") as fp:
        data = yaml.load(fp)
    return data

def resolved_component(
    path: Path, ref: str, specs: dict[Path, dict]
) -> tuple[dict, Path]:
    filename, _, schema_path = ref.partition("#/")
    schema_file = path
    if filename != "":
        schema_file = (path.parent / filename).absolute()
    spec = specs[schema_file]
    for segment in schema_path.split("/"):
        spec = spec[segment]
    return spec, schema_file

def relativize_ref(root: Path, path: Path, ref: str) -> str:
    filename, _, component_path = ref.partition("#/")
    relative_path = path.relative_to(root.parent)
    if filename:
        relative_path = relative_path.parent / filename
    return f"{relative_path}#/{component_path}"

def flattened_properties(
    parent: Path, path: Path, schema: dict, specs: dict[Path, dict], seen: set[int]
) -> tuple[dict[str, Any], set[str]]:
    if schema_properties := schema.get("properties"):
        if parent != path and id(schema_properties) not in seen:
            seen.add(id(schema_properties))
            for property_name, schema_def in schema_properties.items():
                if schema_ref := schema_def.get("$ref"):
                    schema_def["$ref"] = relativize_ref(parent, path, schema_ref)
        return schema_properties, set(schema.get("required", []))
    if all_of := schema.get("allOf"):
        all_properties: dict[str, Any] = {}
        required = set()
        for all_of_schema in all_of:
            if ref := all_of_schema.get("$ref"):
                referenced_schema, schema_file = resolved_component(path, ref, specs)
                resolved_properties, resolved_required = flattened_properties(
                    parent, schema_file, referenced_schema, specs, seen
                )
                all_properties |= resolved_properties
                required |= resolved_required
            elif all_of_properties := all_of_schema.get("properties"):
                all_properties |= all_of_properties
                required.update(all_of_schema.get("required", []))
        return all_properties, required
    raise ValueError(f"Nie obsłużona schema {schema}")

def explode_get_parameters(filename: Path, specs: dict[Path, dict]):
    spec = specs[filename]
    seen_schemas: set[int] = set()
    for operations in spec.get("paths", {}).values():
        modified = False
        get_operation = operations.get("get", {})
        get_parameters = get_operation.get("parameters", [])
        rewritten_parameters = []
        for parameter in get_parameters:
            if "$ref" not in parameter:
                rewritten_parameters.append(parameter)
                continue
            param, _ = resolved_component(filename, parameter["$ref"], specs)
            if param["in"] != "query":
                rewritten_parameters.append(parameter)
                continue
            explode = param.get("explode", False)
            if explode:
                param_schema = param["schema"]
                if "$ref" in param_schema:
                    param_schema, _ = resolved_component(
                        filename, param_schema["$ref"], specs
                    )
                if param_schema.get("type") == "array":
                    rewritten_parameters.append(parameter)
                    continue
                modified = True
                schema_properties, required_properties = flattened_properties(
                    filename, filename, param_schema, specs, seen_schemas
                )
                for k, v in schema_properties.items():
                    new_params = {
                        "name": k,
                        "in": "query",
                        "required": k in required_properties,
                        "schema": v
                    }
                    if description := v.get("description"):
                        new_params["description"] = description
                    rewritten_parameters.append(new_params)
            else:
                rewritten_parameters.append(parameters)
        if modified:
            get_operation["parameters"] = rewritten_parameters


def remove_pageable_parameters(filename: Path, specs: dict[Path, dict]):
    spec = specs[filename]
    for parameter in spec.get("components", {}).get("parameters", {}).values():
        if not parameter.get("explode", False):
            continue
        schema = parameter.get("schema", {})
        if "$ref" in schema:
            schema, _ = resolved_component(filename, schema["$ref"], specs)
        all_of = schema.get("allOf", [])
        for i, all_of_def in enumerate(all_of):
            if all_of_def.get("$ref", "").endswith("PagedParamsOptional"):
                del all_of[i]
                break
    for path, operation in spec.get("paths", {}).items():
        for http_type, operation_def in operation.items():
            if not isinstance(operation_def, dict):
                continue
            parameters = operation_def.get("parameters", [])
            param_num = len(parameters)
            i = 0
            while i < param_num:
                parameter = parameters[i]
                if ref := parameter.get("$ref"):
                    if ref.endswith("/PagedParamsOptional"):
                        print("Usuwanie parametrów stronicowania", http_type, path)
                        del parameters[i]
                        param_num -= 1
                i += 1

def main():
    args = cli()
    specs: dict[Path, dict] = {}
    filename = args.input.absolute()
    parse_openapi(specs, filename)
    spec = specs[filename]

    if args.explode_get_parameters:
        explode_get_parameters(filename, specs)
    if args.remove_pageable_parameters:
        remove_pageable_parameters(filename, specs)

    yaml = YAML()
    yaml.dump(spec, args.output)

if __name__ == "__main__":
    main()
































































