#!/usr/bin/env python3
from __future__ import annotations

import logging
import re
import subprocess
import sys
from argparse import ArgumentParser, ArgumentTypeError, Namespace
from pathlib import Path

def init_logger(level=logging.WARN) -> logging.Logger:
    logger = logging.getLogger(__file__)
    logger.setLevel(logging.DEBUG)
    handler = logging.StreamHandler()
    handler.setLevel(level)
    logger.addHandler(handler)
    return logger

def readable_file_path(filename: str) -> Path:
    path = Path(filename)
    if not path.is_file():
        raise ArgumentTypeError(f"{filename} nie istnieje albo nie jest plikiem")
    return path

LOG = init_logger()

def parse_cli() -> Namespace:
    parser = ArgumentParser()
    parser.add_argument(
        "-f",
        "--file",
        help="Plik, dla którego należy wyliczyć wersję",
        required=True,
        type=readable_file_path,
    )
    return parser.parse_args()

def file_git_version(file: Path, submodule_dirs: list[str]) -> str:
    path = str(file)
    for submodule_dir in submodule_dirs:
        if path.startswith(submodule_dir):
            path = submodule_dir
            break

    res = subprocess.run(
        [
            "git",
            "log",
            "-n",
            "1",
            "--pretty=format:%ct-%h",
            "--abbrev=10",
            "--",
            path,
        ],
        check=True,
        capture_output=True,
    )
    return res.stdout.decode(sys.stdin.encoding).strip()

def git_submodules() -> list[dict[str, str]]:
    res = subprocess.run(["git", "submodule"], check=True, stdout=subprocess.PIPE)
    return list(
            map(parse_submodule_output, res.stdout.decode("utf8").strip().splitlines())
    )

def parse_submodule_output(line: str) -> dict[str, str]:
    columns = line.split()
    tokens = len(columns)
    return {
        "rev": columns[0],
        "path": " ".join(columns[1 : tokens - 1]),
        "head": columns[-1],
    }

def compute_version(file: Path, mark_dirty: bool) -> str:
    src_dir = file.parent
    submodule_dirs = [submodule["path"] for submodule in git_submodules()]
    dirty_files = set()
    if mark_dirty:
        dirty_files = set(
            subprocess.run(
                ["git", "diff", "HEAD", "--name-only", str(src_dir)],
                check=True,
                capture_output=True,
            )
            .stdout.decode(sys.stdin.encoding)
            .strip()
            .split("\n")
        )
        dirty_files.discard("")
    text = file.read_text()
    dependencies = set(re.findall(r"""\$ref:\s+['"]?([^#'"]+)""", text))
    spec_version = ""
    if match := re.search(r"version: '?([\d.]+)'?", text):
        spec_version = match.group(1)
    else:
        raise ValueError(f'Nie odnaleziono "version:" w pliku {file}')
    versions = set([spec_version + "-" + file_git_version(file, submodule_dirs)])

    for dependency in dependencies:
        dep_file = src_dir / dependency
        versions.add(spec_version + "-" + file_git_version(dep_file, submodule_dirs))
    return max(versions)

def main():
    args = parse_cli()
    version = compute_version(args.file, mark_dirty=True)
    print(version)

if __name__ == "__main__":
    main()



































































