import logging
import sys
from argparse import ArgumentParser, BooleanOptionalAction, FileType, Namespace
from itertools import chain

from ruamel.yaml import YAML

def get_logger(level=logging.INFO):
    logger = logging.getLogger(__file__)
    logger.setLevel(level)
    logger.addHandler(logging.StreamHandler())
    return logger

LOGGER = get_logger()

def parse_cli() -> Namespace:
    parser = ArgumentParser()
    parser.add_argument(
        "--file", help="Plik ze specyfikacją", required=True, type=FileType("r")
    )
    parser.add_argument("--out", help="Plik wynikowy", required=True)
    parser.add_argument(
        "--debug",
        help="Czy wypisywać szczegółowe informacje o działaniu",
        default=False,
        action=BooleanOptionalAction,
    )
    return parser.parse_args()

def get_properties(schema: dict):
    yield from schema.get("properties", {}).items()
    yield from chain.from_iterable(
        (
            subschema["properties"].items()
            for subschema in schema.get("allOf", [])
            if "properties" in subschema
        )
    )

def rewrite_openapi(spec: dict):
    has_error = False
    for schema_name, schema in spec.get("components", {}).get("schemas", {}).items():
        for property_name, property_schema in get_properties(schema):
            if "description" in property_schema:
                desc = property_schema["description"].rstrip()
                seperator = ". "
                if desc[-1] in ("?", ".", "!"):
                    separator = " "
                property_schema["description"] = desc + seperator
            else:
                property_schema["description"] = ""
    LOGGER.info("Dodano walidację dla %s pól", 0)
    if has_error:
        sys.exit(1)

def assert_no_special_character(names: list[str]):
    for name in names:
        for c in "!@#$%^&*(){}[]-\"'\\/.,<>":
            if c in name:
                LOGGER.critical("%s nie może zawierać specjalnych znaków", name)
                sys.exit(1)

def main():
    args = parse_cli()
    if args.debug:
        LOGGER.setLevel(logging.DEBUG)
    yaml = YAML()
    yaml.preserve_quotes = True
    spec = yaml.load(args.file)
    args.file.close()
    rewrite_openapi(spec)
    with open(args.out, "w", encoding="utf-8") as fp:
        yaml.dump(spec, fp)

if __name__ == "__main__":
    main()

