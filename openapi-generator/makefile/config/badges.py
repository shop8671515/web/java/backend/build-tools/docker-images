#!/usr/bin/env python3
import json
import sys
from argparse import ArgumentParser
from os.path import join
from pathlib import Path
from sys import argv
from urllib.parse import urljoin, urlparse
from urllib.parse import Request, urlopen

from ruamel.yaml import YAML

API = "https://gitlab.com/api/v4/"

def cmd():
    parser = ArgumentParser(description="Dodaje odznaki z informacją o wersji")
    parser.add_argument("--module", help="Nazwa modułu z API", type=str, required=True)
    parser.add_argument("--token", help="Gitlab API token", type=str, required=True)
    parser.add_argument("--id", help="Gitlab project id", type=int, required=True)
    parser.add_argument(
        "--mvn-repo", help="Nazwa repozytorium mavena", type=str, required=True
    )
    return parser

def main(params: list[str]):
    parser = cmd()
    args = parser.parse_args(params)
    spec_dir = Path("src")
    spec_file = spec_dir / f"{args.module}.openapi.yml"

    if not spec_file.is_file():
        print(
            f"Moduł {args.module} nie istnieje. Spodziewano się znaleźć plik {spec_file}"
        )
        sys.exit(1)

    yaml = YAML()
    with open(f"config/{args.module}-typescript.yml", "r", encoding="utf8") as fp:
        ts_config = yaml.load(fp)

    with open(f"config/{args.module}-spring.yml", "r", encoding="utf8") as fp:
        spring_config = yaml.load(fp)

    url = spring_config["scmUrl"]
    mvn_group = spring_config["groupId"].replace(".", "/")
    mvn_repo = args.mvn_repo
    project_path = urlparse(url).path
    
def add_badge(args, project_id: int, url: str, badge_url: str, name: str):
    assert_badge_file_exists(badge_url)
    api_url = urljoin(API, f"projects/{project_id}/badges")
    data = {"link_url": url, "image_url": badge_url, "name": name}
    payload = json.dumps(data)
    req = Request(
        api_url, data=payload.encode("utf8"), headers=post_headers(args.token)
    )
    with urlopen(req) as _:
        pass

def assert_badge_file_exists(url: str):
    with urlopen(url) as _:
        pass

def post_headers(token):
    return {"Private-Token": token, "Content-Type": "application/json"}

if __name__ == "__main__":
    main(argv[1:])
