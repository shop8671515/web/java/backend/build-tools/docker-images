SHELL := bash
DOCKER_CONTEXT := .
DOCKER_GROUP := bananawhite98
VERSION = 

image:
	docker build --pull -f $(shell echo $(DOCKER_IMAGE_FULL) | cut -d'/' -f2 | cut -d':' -f1)/Dockerfile -t $(DOCKER_IMAGE_FULL) $(DOCKER_CONTEXT)

docker-push:
	docker push $(DOCKER_IMAGE_FULL)

docker-rmi:
	docker rmi -f $(DOCKER_IMAGE_FULL)

build-%: 
	$(MAKE) image DOCKER_IMAGE_FULL=$(DOCKER_GROUP)/$*:$(VERSION)
push-%:
	$(MAKE) docker-push DOCKER_IMAGE_FULL=$(DOCKER_GROUP)/$*:$(VERSION)
rmi-%:
	$(MAKE) docker-rmi DOCKER_IMAGE_FULL=$(DOCKER_GROUP)/$*:$(VERSION)


